const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const httpErrors = require('http-errors');
const cors = require('cors');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const restaurantRouter = require('./routes/restaurant');
const platRouter = require('./routes/plat');
const avisRouter = require('./routes/avis');

const app = express();

// ********************
// SETUP ENGINE *******
// ********************

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(cookieParser());
app.use(helmet());
app.use(bodyParser.json());
app.use(cors());


// ********************
// DEFINE ROUTES ******
// ********************

app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/restaurant', restaurantRouter);
app.use('/restaurant', avisRouter);
app.use('/restaurant', platRouter);

// ********************
// GLOBAL ERROR 404 ***
// ********************

app.use(function (req, res, next) {
    next(httpErrors(404));
});

// Configuration pour le reverse proxy
app.set('trust proxy', 'loopback');

module.exports = app;
