'use strict';
module.exports = (sequelize, DataTypes) => {
  const Avis = sequelize.define('Avis', {
    content: DataTypes.TEXT('long'),
    note: DataTypes.STRING,
    pseudo: DataTypes.STRING,
    date: DataTypes.STRING
  }, {});
  Avis.associate = function (models) {
    Avis.belongsTo(models.Restaurant, { foreignKey: "fk_restaurantID", as: "Restaurant" })
  };
  return Avis;
};