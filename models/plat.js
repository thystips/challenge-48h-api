'use strict';
module.exports = (sequelize, DataTypes) => {
  const Plat = sequelize.define('Plat', {
    type: DataTypes.STRING,
    titre: DataTypes.STRING,
    prix: DataTypes.STRING
  }, {});
  Plat.associate = function (models) {
    Plat.belongsTo(models.Restaurant, { foreignKey: "fk_restaurantID", as: "Restaurant" })
  };
  return Plat;
};