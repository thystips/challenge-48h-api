'use strict';
module.exports = (sequelize, DataTypes) => {
  const Restaurant = sequelize.define('Restaurant', {
    type: DataTypes.STRING,
    nom: DataTypes.STRING,
    adresse: DataTypes.STRING,
    prixmoyen: DataTypes.STRING,
    note: DataTypes.STRING,
    information: DataTypes.TEXT('long')
  }, {});
  Restaurant.associate = function (models) {
  };
  return Restaurant;
};