# CHANGELOG

<!--- next entry here -->

## 1.1.6
2020-05-15

### Fixes

- **code:** fix code error in doc (c5b8b3586238603156b165bd3bcf5e3b2f1b8c18)

## 1.1.5
2020-05-15

### Fixes

- **datatypes:** text long pour content avis (cf1b51300021a79dfdfcd88c154138215a3e8ca8)

## 1.1.4
2020-05-15

### Fixes

- **datatypes:** Long text pour information (416d22205a9de74155da4b9a1c343a5bb1d6f321)

### Other changes

- Update README.md (4d527128baad393b41cf0b43c6ae964398638822)
- Merge branch 'master' of gitlab.com:antoine33520/challenge-48h-api (dd6cc2b8460368d1a6ac2bcc4bb1206a62041ee2)

## 1.1.3
2020-05-15

### Fixes

- **readme:** starting write readme.md (264b6bd34b604702275a489bac06b837c960ee65)

### Other changes

- version bump for v1.1.2 [skip ci] (c0a6d39dc71bf78041c1890159e9393498332014)

## 1.1.2
2020-05-15

### Fixes

- **controlleurs:** Mausvais message de réponse pour http code 204 (d47ba09ce6ea8a0b5d603e744abf9088a34fc193)
- **dependencies:** Add mysql depencie for production (2f1ff366d0b52f97dead8e8177b2b03d0fe008e9)

### Other changes

- Merge branch 'master' of gitlab.com:antoine33520/challenge-48h-api (fac360ed1ef69ff07b1c84c0f702c2f372c16949)
- Merge branch 'master' of gitlab.com:antoine33520/challenge-48h-api (38afd110046c36765627d882ed917f8a40528a69)

## 1.1.1
2020-05-15

### Fixes

- **controlleurs:** status 204 n'envois aps de réponse (e1f4011cbf022bf36d2723eea9a16c059c9ba69d)

## 1.1.0
2020-05-15

### Features

- **deployment:** Update compose file on pipeline (9087f19e3d137b7e7bbdd3fadb534f1a1ca5ba0e)

### Fixes

- **db:** Change db profile definition (16c08d5ac66d8ea839f4978c072317856c302474)

## 1.0.8
2020-05-15

### Fixes

- **conatiner:** use 1 instead true (49caf6bebc0d07141e6423ff236de280c562605d)
- **ci:** Remove .npm from cache (2ae44b37b5162313e8d39c3e54b81192b7d38b66)

## 1.0.7
2020-05-15

### Fixes

- **ci:** force prunning images (07adb0d45daf23e58bc7eb0b9963c2fed7d459e1)

### Other changes

- version bump for v1.0.6 [skip ci] (642e0989fedda73e925fea6a2ab3fefce71d6cbf)

## 1.0.6
2020-05-15

### Fixes

- **container:** try to repair image (d138c4c2d34114ec028a37baf634180fb5bf9a35)
- **db:** Localisation de DB sqlite & ajout commande pour lancer en production avec NPM (96f3a5a86620f16401d2135f7a410e2372f98a2d)

### Other changes

- Merge branch 'master' of gitlab.com:antoine33520/challenge-48h-api (200c65eac8df6562fa701a5ea403a38c87bc1da7)

## 1.0.5
2020-05-15

### Fixes

- **controllers:** ajout status controller (232c1e8e1c1ad751509d40ba0b3ee4a1afa1589a)

### Other changes

- Merge branch 'master' of gitlab.com:antoine33520/challenge-48h-api (96d252d71b4d6c3e100dd28fc6acd46f54e1bb7d)

## 1.0.4
2020-05-14

### Fixes

- **container:** Repair env var causing crash on startup (65ea2ee759f72d815717c21de93086ff2ba18ce4)

### Other changes

- Merge branch 'master' of gitlab.com:antoine33520/challenge-48h-api (72ae1593033125f8893b2915b364046ff390dd11)

## 1.0.3
2020-05-14

### Fixes

- **package.js:** Add newline (8b4f479e0ea76d445e3d95c5d82141d394f829b9)

## 1.0.2
2020-05-14

### Fixes

- **dependencies:** All dependencies to satisfies version (8d7f7c31e3b39fc7dabc27b3e0b784786db57322)

## 1.0.1
2020-05-14

### Fixes

- **container:** Install only prod dependencies & Upercase env var (da05191cc442e80585af68dcf6c5811c996b1921)
- **dependencies:** Update Express (d22c40d4ff835db136c2d56451b32f6afe8dfa41)

## 1.0.0
2020-05-14

### Features

- **server:** Add reverse proxy configuration (a68abeefff2c7e55e69007db2a5390a4232645b1)
- **controllers:** Ajout controllers (ee7f2e5a0acafb419ffa7ff4acbe2f3d9f724103)
- **models:** Ajout models (110d3a97fff21dc6c85d92a59b66aa2f15802aac)
- **routes:** Ajout routes (930e2613d2e355219eb43d68fcfbc45b11c060d2)
- **middlewares:** Ajout middlewares (3f16153eb1e894d163625c231f2db246aa86a963)
- **migrations:** Ajout migrations (064e136d001e52c633282da4d707a26dd1a2975f)
- **db:** Ajout config DB. (7fdfd75a867f6208b2af491eebdb34eb61d16926)
- **express:** Modifications de la config de Express (d0188e2e23189ff2ece8f312dfcc354c93951d4c)
- **npm:** ajout de packets (36a13dd5147b309ccad03529706689ae45ba70f0)
- **db:** Add db config (04f23edb23bf19377c907161234e3f8473a84158)
- **container:** Create Dockerfile for project building (c961022ad55b1d41fa0def0cb0bc7d6fe9e97b50)
- **ci:** Add versionning and deployment in Gitlab CI (d9dac714b697925c6f9f40f8e1d632b104cbed98)
- **controllers:** Create All controllers (3976ab0d94017924f915075b7527509821e870cb)
- **licence:** Add Open Source licence (266bfa08216a7d137baa06b9eb137fda9d261a3d)

### Fixes

- **vscode:** Add workspace (359750dfedaeba0d21166dbeb42a212f5d3ba90b)
- **git:** Add .gitignore (b61156e48c1504851f4b066ca320d8a58150d852)
- **dependencies:** Clean node_modules on repo (cd72d551d9df96176312a43ce7a5b54fe639a2da)
- **dev:** Change version and updates for dev (c6a2e9a2f6eb067bf0f5fb72f66acdf8443c4afc)
- **controllers:** Finish controllers (0806a774b3a26fcd4e111e63f3a7392ad04787ae)
- **models:** Add date & foreign key (69efcecb4d2ff9d3393f27c7f52f94822a95d224)
- **routes:** Finish all routes (ee3d0549914903c741547c8dcd11d91da2303b7d)
- **versionning:** Add other changes in CHANGELOG (87173a54779b2f03296b532dff01a20204fb286b)
- **ci:** Disable TLS (19a58470c992676ff0e202f81e40b3dbc338b3f2)
- **ci:** Remove useless commands (e67db95ec832dc35e5da8b1d0f29cacc1042ee62)

### Other changes

- Initial commit (0559a9f3836300628e2b79841323f0f8bc0b459c)