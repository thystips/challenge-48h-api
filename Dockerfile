FROM node:14

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . .

ENV NODE_ENV=production

RUN npm install --only=prod

EXPOSE 3000

CMD ["node", "./bin/www"]
