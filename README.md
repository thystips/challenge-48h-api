# Challenge 48h API

Documentation du service API du projet [Bordeaux Resto](https://gitlab.com/antoine33520/challenge-48h)

## Sommaire

- [Challenge 48h API](#challenge-48h-api)
  - [Sommaire](#sommaire)
    - [Présentation](#présentation)
    - [Dépôt Git](#dépôt-git)
  - [Initialisation du projet](#initialisation-du-projet)
    - [Pré-requis](#pré-requis)
  - [Installation du projet](#installation-du-projet)
    - [Clonage du dépôt git](#clonage-du-dépôt-git)
    - [Installation des dépendances](#installation-des-dépendances)
    - [Lancement du projet et du serveur local](#lancement-du-projet-et-du-serveur-local)
    - [Lien du serveur en local](#lien-du-serveur-en-local)
  - [Architecture du projet](#architecture-du-projet)
    - [Le scrap](#le-scrap)
    - [Le serveur](#le-serveur)
    - [Les models](#les-models)
    - [Les controllers](#les-controllers)
  - [Les fonctionnalités de notre projet](#les-fonctionnalités-de-notre-projet)
    - [Méthode Get](#méthode-get)
    - [Méthode Create](#méthode-create)
    - [Méthode Delete](#méthode-delete)
    - [Méthode Update](#méthode-update)
  - [Sites utiles](#sites-utiles)

### Présentation

Le sous module Challenge 48h API est une partie du projet [Bordeaux Resto](https://gitlab.com/antoine33520/challenge-48h)

### Dépôt Git

[Lien de notre dépôt git pour ce service](https://gitlab.com/antoine33520/challenge-48h-api)

## Initialisation du projet

### Pré-requis

**Software**

- Node js
- NPM
- Sqlite3

## Installation du projet

### Clonage du dépôt git

Normalement si vous avez cloné le dépôt global du projet en suivant les instruction il faut suffit d'être dans le dossier API.
Sinon vous pouvez cloner le dépôt de ce service via les commandes :

* En SSH :

    ```bash
    $ git clone git@gitlab.com:antoine33520/challenge-48h-api.git
    ```

* En HTTPS :

    ```
    $ git clone https://gitlab.com/antoine33520/challenge-48h-api.git
    ```

### Installation des dépendances

```
$ npm install
```

### Lancement du projet et du serveur local

```
$ node bin/www
```

### Lien du serveur en local

```
$ http://localhost:3000
```

## Architecture du projet

### Le scrap

Le script python `scrap.py` permet de remplir la base de données avec les données du site **La Fourchette**.

### Le serveur

Le fichier comprenant toutes les routes de notre projet.

### Les models

Les models constituent l'initialisation du squelette d'une table que **Sequelize** utilise pour y faire ses traitements dessus. 

### Les controllers

Les controllers permettent de réunir toutes les méthodes utilisées pour chacune des **tables**.

## Les fonctionnalités de notre projet

### Méthode Get 

Cette méthode permet de faire une demande au serveur pour récuperer les données générales de chaque table avec les onglets sur *la navbar* ou de récupérer seulement une partie à l'aide de notre barre de recherche.

### Méthode Create

Méthode permettant de créer un nouveau *Joueur*, *Jeu* ou *Runs* à l'aide d'un bouton ouvrant un formulaire.

### Méthode Delete

La méthode delete est utilisée pour supprimer une donnée que l'on filtre par *ID* ou par *Nom*.

### Méthode Update

Le rôle de la methode update a pour fonctionnalité de mettre à jour une donnée dans la base de données.

## Sites utiles
 
[Sequelize](https://sequelize.org/master/manual/getting-started.html)  
[Express](http://expressjs.com/en/starter/installing.html)  
[Path](https://nodejs.org/api/path.html)  
[Body-Parser](https://www.npmjs.com/package/body-parser)  
[Bootstrap](https://getbootstrap.com/docs/4.4/getting-started/introduction/)
