var express = require('express');
var router = express.Router();
const restaurantController = require('../controllers/restaurantController');
const auth = require('../middlewares/auth');


router.get('/',
    restaurantController.getAll
);

router.post('/create',
    restaurantController.create
)

router.put('/update/:id',
    restaurantController.update
);

router.delete('/delete/:id',
    restaurantController.delete
);

router.get('/:id',
    restaurantController.get
);

module.exports = router;

