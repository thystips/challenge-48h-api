var express = require('express');
var router = express.Router();
const platController = require('../controllers/platController');
const restaurantController = require('../controllers/restaurantController');
const auth = require('../middlewares/auth');

router.get('/:idRestaurant/plat',
    restaurantController.getNext,
    platController.getAll
);

router.put('/:idRestaurant/plat/update/:idPlat',
    restaurantController.getNext,
    platController.update
);

router.delete('/:idRestaurant/plat/delete/:idPlat',
    restaurantController.getNext,
    platController.delete
);

router.post('/:idRestaurant/plat/create',
    restaurantController.getNext,
    platController.create
);

module.exports = router;
