var express = require('express');
var router = express.Router();
const avisController = require('../controllers/avisController');
const restaurantController = require('../controllers/restaurantController');
const auth = require('../middlewares/auth');


router.get('/:idRestaurant/avis',
    restaurantController.getNext,
    avisController.getAll
);

router.put('/:idRestaurant/avis/update/:idAvis',
    restaurantController.getNext,
    avisController.update
);

router.delete('/:idRestaurant/avis/delete/:idAvis',
    restaurantController.getNext,
    avisController.delete
);

router.post('/:idRestaurant/avis/create',
    restaurantController.getNext,
    avisController.create
);

module.exports = router;

