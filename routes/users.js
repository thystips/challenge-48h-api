var express = require('express');
var router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../middlewares/auth');


router.get('/',
  userController.getAll
);

router.post('/login',
  userController.login
);

router.get('/current',
  auth,
  userController.current
);

router.get('/logout',
  auth,
  userController.logout
);

router.post('/register',
  userController.create
);

router.put('/update',
  auth,
  userController.update
);

router.delete('/delete',
  auth,
  userController.logoutNext,
  userController.delete
);

router.get('/:id',
  userController.get
);

module.exports = router;

