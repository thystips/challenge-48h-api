const Avis = require('../models').Avis;
const { Op } = require('sequelize');

exports.getAll = (req, res, next) => {

    const restaurant = res.locals.restaurant;

    let limit = req.query.limit ?
        (
            isNaN(req.query.limit) ?
                20
                :
                req.query.limit
        )
        : 20;


    let offset = req.query.offset ?
        (
            isNaN(req.query.offset) ?
                0
                :
                req.query.offset
        )
        : 0;

    let titre = req.query.s ? req.query.s : "";

    Avis.findAll({
        where: {
            fk_restaurantID: {
                [Op.eq]: restaurant.id
            }
        },
        limit: limit,
        offset: offset,
        order: [
            ['date']
        ],
        raw: true,
        nest: true
    })
        .then(result =>
            result.length > 0 ?
                res.status(200).json({
                    data: result,
                    offset: offset,
                    limit: limit
                })
                :
                res.status(203).json({
                    message: "Aucunes données"
                })
        )
        .catch(error => {
            res.status(500).json({
                message: "Une erreur est survenue. "
            });
        }
        );

};

exports.create = (req, res, next) => {

    const restaurant = res.locals.restaurant;

    Avis.create({
        content: req.body.content,
        note: req.body.note,
        pseudo: req.body.pseudo,
        date: req.body.date,
        fk_restaurantID: restaurant.id
    })
        .then(result => {
            res.status(201).json({
                avis: result.dataValues
            });
        })
        .catch(error => {
            res.status(500).json({
                message: "Une erreur est survenue. "
            });
            console.log(error);

        }
        );

};

exports.delete = (req, res, next) => {

    Avis.destroy({
        where: {
            id: {
                [Op.eq]: req.params.idAvis
            }
        }
    })
        .then(result =>
            res.status(200).json({
                message: "Avis supprimé"
            })
        )
        .catch(error => res.status(500).json({
            message: "Une erreur est survenue. "
        })
        );

};

exports.update = (req, res, next) => {

    const restaurant = res.locals.restaurant;

    Avis.update(
        {
            content: req.body.content,
            note: req.body.note,
            pseudo: req.body.pseudo,
            date: req.body.date,
            fk_restaurantID: restaurant.id
        },
        {
            where: {
                id: {
                    [Op.eq]: req.params.idAvis
                }
            }
        })
        .then(result =>
            result[0] ?
                res.status(201).json({
                    message: "Avis mis à jour"
                })
                :
                res.status(203).json({
                    message: "Aucunes correspondances"
                })
        )
        .catch(error => res.status(500).json({
            message: "Une erreur est survenue. "
        })
        );
};



