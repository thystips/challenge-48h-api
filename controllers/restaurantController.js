const Restaurant = require('../models').Restaurant;
const { Op } = require('sequelize');

exports.getAll = (req, res, next) => {

    let limit = req.query.limit ?
        (
            isNaN(req.query.limit) ?
                20
                :
                req.query.limit
        )
        : 20;


    let offset = req.query.offset ?
        (
            isNaN(req.query.offset) ?
                0
                :
                req.query.offset
        )
        : 0;

    let restaurant = req.query.s ? req.query.s : "";

    Restaurant.findAll({
        where: {
            nom: {
                [Op.like]: "%" + restaurant + "%"
            }
        },
        limit: limit,
        offset: offset,
        order: [
            ['nom']
        ],
        raw: true,
        nest: true
    })
        .then(result =>
            result.length > 0 ?
                res.status(200).json({
                    data: result,
                    offset: offset,
                    limit: limit
                })
                :
                res.status(203).json({
                    message: "Aucunes données"
                })
        )
        .catch(error => {
            res.status(500).json({
                message: "Une erreur est survenue. "
            });
            console.log(error);

        }
        );

};

exports.get = (req, res, next) => {

    Restaurant.findOne({
        where: {
            id: {
                [Op.eq]: req.params.id
            }
        }
    })
        .then(result =>
            result ?
                res.status(200).json({
                    data: result
                })
                :
                res.status(203).json({
                    message: "Aucunes correspondances"
                })
        )
        .catch(error => res.status(500).json({
            message: "Une erreur est survenue. "
        })
        );

};

exports.getNext = (req, res, next) => {

    Restaurant.findOne({
        where: {
            id: {
                [Op.eq]: req.params.idRestaurant
            }
        }
    })
        .then(result => {
            if (result) {
                res.locals.restaurant = result.dataValues;
                next();
            } else {
                res.status(203).json({
                    message: "Ce restaurant n'existe pas."
                });
            }
        }
        )
        .catch(error => res.status(500).json({
            message: "Une erreur est survenue. "
        })
        );

};

exports.getOne = (userId) => {

    return User.findOne({
        where: {
            id: {
                [Op.eq]: userId
            }
        }
    });

};

exports.create = (req, res, next) => {

    Restaurant.create({
        type: req.body.type,
        nom: req.body.nom,
        adresse: req.body.adresse,
        prixmoyen: req.body.prixmoyen,
        note: req.body.note,
        information: req.body.information
    })
        .then(result => {
            res.status(201).json({
                restaurant: result.dataValues
            });
        })
        .catch(error => res.status(500).json({
            message: "Une erreur est survenue. "
        })
        );

};

exports.delete = (req, res, next) => {

    Restaurant.destroy({
        where: {
            id: {
                [Op.eq]: req.params.id
            }
        }
    })
        .then(result =>
            res.status(200).json({
                message: "Restaurant supprimé"
            })
        )
        .catch(error => res.status(500).json({
            message: "Une erreur est survenue. "
        })
        );

};

exports.update = (req, res, next) => {

    Restaurant.update(
        {
            type: req.body.type,
            nom: req.body.nom,
            adresse: req.body.adresse,
            prixmoyen: req.body.prixmoyen,
            note: req.body.note,
            information: req.body.information
        },
        {
            where: {
                id: {
                    [Op.eq]: req.params.id
                }
            }
        })
        .then(result =>
            result[0] ?
                res.status(201).json({
                    message: "Restaurant mis à jour"
                })
                :
                res.status(203).json({
                    message: "Aucunes correspondances"
                })
        )
        .catch(error => res.status(500).json({
            message: "Une erreur est survenue. "
        })
        );
};
