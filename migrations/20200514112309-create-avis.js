'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Avis', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      content: {
        type: Sequelize.STRING
      },
      note: {
        type: Sequelize.STRING
      },
      pseudo: {
        type: Sequelize.STRING
      },
      date: {
        type: Sequelize.DATE
      },
      fk_restaurantID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        reference: {
          model: 'Restaurant',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Avis');
  }
};