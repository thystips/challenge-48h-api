const jwt = require('jsonwebtoken');
const controller = require('../controllers/userController');

module.exports = (req, res, next) => {
    try {

        const token = req.cookies.authToken;
        const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
        const userId = decodedToken.userId;

        if (req.body.userId && req.body.userId !== userId) {
            throw "Authentification invalide";
        } else {
            controller.getOne(userId)
                .then(
                    value => {
                        res.locals.authUserCurrent = value.dataValues;
                        next();
                    }
                )
                .catch(
                    reason => console.log(reason)
                );
        }

    } catch (e) {
        res.status(401).json(
            {
                code: 401,
                message: new Error("Requête invalide !"),
                error: e
            }
        );
    }
};
